<?php
class Assignment1 
{
	function check_consecutive_occurance($input_string_param = '')
	{
		$input_string 	= htmlentities(strip_tags($input_string_param));
		$counter 		= 0;

		if(strlen($input_string) == 1) 
		{
			return "Good luck!";
		} 

		for($index_counter = 0; $index_counter < strlen($input_string_param)-2; $index_counter++)
		{
			if($input_string_param[$index_counter] == $input_string_param[$index_counter+1])
			{
				$counter++;
			}
			else 
			{
				$counter = 0;
			}

			if($counter == 5) break;
		}

		if($counter == 5)
		{
			return "Sorry, sorry!";
		}
		else
		{
			return "Good luck!";
		}
	}
}

$assignment_one	= new Assignment1();
echo $assignment_one->check_consecutive_occurance('0001111110');